#!/usr/bin/make -f
-include /usr/share/gnome-pkg-tools/1/rules/gnome-get-source.mk

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

PYTHON_CPPFLAGS_PKGCONFIG := $(shell pkg-config --cflags python3)
PYTHON_LIB_PKGCONFIG := $(shell pkg-config --libs python3)

%:
	dh $@ --with gnome,python3

override_dh_clean:
	rm -f debian/glom.1
	dh_clean

override_dh_auto_configure:
	dh_auto_configure -- \
		--disable-update-mime-database \
		--with-postgres-utils=/usr/lib/postgresql/11/bin \
		PYTHON_CPPFLAGS="$(PYTHON_CPPFLAGS_PKGCONFIG)" \
		PYTHON_LIBS="-L/usr/lib/$(DEB_HOST_MULTIARCH) $(PYTHON_LIB_PKGCONFIG)" \
		PYTHON=python3 PYTHON_VERSION=3

override_dh_auto_build:
	dh_auto_build
	/usr/bin/docbook-to-man debian/glom.sgml > debian/glom.1

# Don't fail the build for test failures yet
override_dh_auto_test:
	-xvfb-run dh_auto_test

override_dh_compress:
	dh_compress -X.glom -X.js

override_dh_install-arch:
	rm -f debian/*/usr/share/mime/{subclasses,globs,magic,XMLnamespaces,aliases,mime.cache}
	rm -f debian/*/usr/lib/python*/*-packages/glom*.la
	rm -f debian/*/usr/lib/python*/*-packages/glom*.a
	rm -f debian/*/usr/share/pyshared/glom*.a
	rm -f debian/*/usr/share/pyshared/glom*.la
	dh_python3 -p glom
	dh_install -a

override_dh_install-indep:
	find debian/tmp -type f -name jquery.js -print -delete
	find debian/tmp -type f -name underscore.js -print -delete
	ln -sf /usr/share/javascript/jquery/jquery.js \
		debian/tmp/usr/share/doc/pyglom_1_30/reference/html/_static/jquery.js
	ln -sf /usr/share/javascript/underscore/underscore.js \
		debian/tmp/usr/share/doc/pyglom_1_30/reference/html/_static/underscore.js
	dh_install -i

override_dh_missing:
	dh_missing --list-missing
