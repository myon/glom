<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

<!-- Process this file with docbook-to-man to generate an nroff manual
     page: `docbook-to-man manpage.sgml > manpage.1'.  You may view
     the manual page with: `docbook-to-man manpage.sgml | nroff -man |
     less'.  A typical entry in a Makefile or Makefile.am is:

manpage.1: manpage.sgml
	docbook-to-man $< > $@

    
	The docbook-to-man binary is found in the docbook-to-man package.
	Please remember that if you create the nroff version in one of the
	debian/rules file targets (such as build), you will need to include
	docbook-to-man in your Build-Depends control field.

  -->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Murray</firstname>">
  <!ENTITY dhsurname   "<surname>Cumming</surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>June 15, 2005</date>">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>dh@mailempfang.de</email>">
  <!ENTITY dhusername  "Daniel Holbach">
  <!ENTITY dhucpackage "<refentrytitle>GLOM</refentrytitle>">
  <!ENTITY dhpackage   "glom">

  <!ENTITY debian      "<productname>Ubuntu</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "&gnu; <acronym>GPL</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2005</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>database designer and user interface</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para>This manual page documents briefly the
      <command>&dhpackage;</command> command.</para>

    <para>This manual page was written for the &debian; distribution
      because the original program does not have a manual page.
    </para>

    <para>With <command>&dhpackage;</command> you can design table 
       definitions and the relationships between them, plus arrange the 
       fields on the screen. You can edit and search the data in those 
       tables, and specify field values in terms of other fields. It's as 
       easy as it should be. 
    </para><para>
       The design is loosely based on FileMaker Pro, with the added advantage 
       of separation between interface and data. Its simple framework should 
       be enough to implement most database applications. Without Glom these 
       systems normally consist of lots of repetitive, unmaintainable code.
    </para>

  </refsect1>
  <refsect1>
    <title>OPTIONS</title>

    <para>These programs follow the usual &gnu; command line syntax,
      with long options starting with two dashes (`-').  A summary of
      options is included below.</para>

    <variablelist>
      <varlistentry>
        <term><option>-?</option>
          <option>--help</option>
        </term>
        <listitem>
          <para>Show summary of options.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-V</option>
          <option>--version</option>
        </term>
        <listitem>
          <para>Show version of program.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-f</option>
          <option>--file</option>
        </term>
        <listitem>
          <para>Filename of the glom project to open.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--debug_sql</option>
        </term>
        <listitem>
          <para>Show the generated SQL queries on stdout, for debugging.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--display=DISPLAY</option>
        </term>
        <listitem>
          <para>X display to use</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
  <refsect1>
    <title>AUTHOR</title>

    <para>This manual page was written by &dhusername; &dhemail; for
      the &debian; system (but may be used by others).  Permission is
      granted to copy, distribute and/or modify this document under
      the terms of the &gnu; General Public License, Version 2 any 
	  later version published by the Free Software Foundation.
    </para>
	<para>
	  On Ubuntu and Debian systems, the complete text of the GNU General 
	  Public License (version 2) can be found in 
	  /usr/share/common-licenses/GPL-2.
	</para>

  </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->
